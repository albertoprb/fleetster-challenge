/*global require*/
'use strict';

// Require.js allows us to configure shortcut alias
require.config({
  // The shim config allows us to configure dependencies for
  // scripts that do not call define() to register a module
  shim: {
    underscore: { exports: '_'},
    backbone: { deps:['underscore', 'jquery'], exports: 'Backbone'},
    backboneLocalstorage: { deps:['backbone'], exports: 'Store'},
    gritter: { deps:['jquery'], exports: 'Gritter'},
    jqueryui: { deps:['jquery'], exports: 'jqueryui'},
    bootstrap: { deps:['jquery'], exports: 'bootstrap'},
    datepicker: { deps:['jquery', 'bootstrap'], exports: 'datepicker'},
    breakpoint: { deps:['jquery'], exports: 'breakpoint'},
    slimscroll: { deps:['jquery', 'jqueryui'], exports: 'slimscroll'},
    blockui: { deps:['jquery'], exports: 'blockui'},
    cookie: { deps:['jquery'], exports: 'cookie'},
    uniform: { deps:['jquery'], exports: 'uniform'},
    validation: { deps:['jquery'], exports: 'validation'},
    select2: { deps:['jquery'], exports: 'select2'},
    datatable: { deps:['jquery'], exports: 'datatable'},
    dtbootstrap: { deps:['jquery'], exports: 'dtbootstrap'},
    helper: { deps:['jquery','breakpoint','blockui'], exports: 'helper'}
  },
  paths: {
    jquery: '../vendor/jquery-1.8.3.min',
    jqueryui: '../vendor/jquery-ui/jquery-ui-1.10.1.custom.min',
    bootstrap: '../vendor/bootstrap/js/bootstrap.min',
    breakpoint: '../vendor/breakpoints/breakpoints',
    slimscroll: '../vendor/jquery-slimscroll/jquery.slimscroll.min',
    blockui: '../vendor/jquery.blockui',
    cookie: '../vendor/jquery.cookie',
    uniform:'../vendor/uniform/jquery.uniform.min',
    validation:'../vendor/jquery-validation/dist/jquery.validate.min',
    select2:'../vendor/select2/select2.min',
    datatable:'../vendor/data-tables/jquery.dataTables',
    dtbootstrap:'../vendor/data-tables/DT_bootstrap',
    gritter:'../vendor/gritter/js/jquery.gritter.min',
    underscore: '../vendor/underscore/underscore',
    backbone: '../vendor/backbone/backbone',
    backboneLocalstorage: '../vendor/backbone.localStorage/backbone.localStorage',
    text: '../vendor/requirejs-text/text',
    serialize: '../vendor/backbone.syphon.min',
    datepicker: '../vendor/bootstrap-datepicker/js/bootstrap-datepicker',
    helper: 'helpers/main.js'
  }
});

require([
  'backbone',
  'bootstrap',
  'routers/router',
  'common'
], function (Backbone, Boostrap, AppRouter, Common) {

  /**
   * Initiate router and immediately call index action
   */
  new AppRouter().index();
  Backbone.history.start();
});
