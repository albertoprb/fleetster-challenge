The frontend is a Javascript + HTML + CSS application that used the following technologies

* RequireJS for script and template loading
* Backbone for structure
* Jquery and Underscore for utilities
* Jquery validation for form validation
* Backbone syphon for form serialization
* Bootstrap 
* Bootstrap date-picker for RichUI
* Gritter for notifications


The code is structured as following

### index.html

The mains layout for login. After successfuly loged in, the user is redirected to app.html

### app.html

The mains layout for application

### main.js

Here we declare all files to be used as dependecies later, and we initiate the router, calling immediately for the index action. 

### common.js

This is our global object for configuration. We used it for storing the logged in user too. It's retrieved and store here as long as there is no refresh.

**Pitfall**

Ideally, the user have to be stored on a cookie we do not lose 'state' between refreshs. However as this is a single page app, this is not a Big issue but it is necessary for production

### routers/router.js

Every route is defined here. There is two helper functions. One is authenticate and the other is change view. 
The first return if the user has a token, and the second remove the current view and its events and set a new one. 

**Pitfall**

*I run into the issue of the separation of view and model creation. I'm a novice in Backbone but I think it's good practice to separete model creation from the view initialize/render. I noticed this in the final part of my work, so I decided to mantain this separation as much as possible*  
  
*There was a strange behaviour on journey list rendering. The journeys were being added to the screen only after three saves. After some debugging I suspected it was something with cache, and after a cache:false option on fetch, it worked as expected*  
  
*I fetch the up to date Journey list on every render of this view. I think that's not a good approach even if I had paginate.*  
  
### Resources

For every resource we have this combination

* models/{resource}.js
* [Optional] collections/{resources}.js
* views/{resource}/view1.js
* ...
* views/{resource}/viewN.js
* templates/{resource}/template1.html
* ...
* templates/{resource}/templateN.html

So I have a one to one correspondence between view and templates.

We have this to resources: User and Journey.

I have a view to the topNavigation and possible a future view to sideNavigation.

### Validation

In the begginning I used the built-in validate method of Backbone models. But I think that jQuery.validation for form validation is better suited and more pratical. 

### Conclusion

Without a doubt the Frontend toke most of my work. I liked the non opinated approach of backbone very much. I had some issues along the way like dependecy management, caching, understanding backbone in the async nature of js, among others.

