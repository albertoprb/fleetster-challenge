<?php

class UsersController extends AppController {

  /**
   * @param int $length Desired Length of the token
   * @return string token
   */
  private function generateRandomString($length = 16) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
  }

  /**
   * POST /users.json
   *
   * This action is used to add a new user. We generate a new token and give it to the recently created user.
   * If the user is created with seccess, an email is sent to the provided email.
   *
   */
  public function add() {
    $this->request->data['token'] = $this->generateRandomString();
    $saved = $this->User->save($this->request->data);

    if ($saved) {
      $this->sendNewUser($saved['User']);
      $this->response->statusCode(200);
      $message = 'Success';
      $id = $this->User->getLastInsertId();

    } else {
      $this->response->statusCode(400);
      $message = 'Error';
      $id = null;
    }

    $this->set(array(
      'message' => $message,
      'id' => $id,
      '_serialize' => array('message', 'id')
    ));
  }

  /**
   * GET /users/show.json?token={token}
   */
  public function show() {
    $token = $this->request->query['token'];
    $user = $this->User->findByToken($token);
    $user['User']['password'] = ""; // Not sending password to the client

    if(empty($user)){
      $this->response->statusCode(404);
      $this->set('responseText', 'User not found');
    }else {
      $this->set('responseText', Set::map($user));
    }

    $this->set('_serialize', 'responseText');
  }

  /**
   * GET /users/update.json?token={token}
   */
  public function update() {
    $token = $this->request->data['token'];
    $user = $this->User->findByToken($token);
    $this->User->id = $user['User']['id'];
    if ($this->User->save($this->request->data)) {
      $this->response->statusCode(200);
      $message = 'Success';
    } else {
      $this->response->statusCode(400);
      $message = 'Error';
    }
    $this->set(array(
      'message' => $message,
      '_serialize' => array('message')
    ));
  }


  /**
   * POST /users/auth.json
   * This action return the token with a HTTP 200 code if the passed json with user/password is valid.
   * Otherwise it returns HTTP 403 (Forbidden).
   */
  public function auth() {
    $username_received = $this->request->data['username'];
    $password_received = $this->request->data['password'];

    $user = $this->User->findByUsername($username_received);

    $token = 'not-authorized';

    if(!empty($user)){
      if($password_received == $user['User']['password']){
        $message = 'Welcome, you are being redirected.';
        $token = $user['User']['token'] ;
        $this->response->statusCode(200);
      }
      else{
        $message = 'Your username or password is not valid. Please signup first!';
        $this->response->statusCode(403);
      }
    } else{
      $message = 'Your username or password is not valid. Please signup first!';
      $this->response->statusCode(403);
    }
    $this->set(array(
      'message' => $message,
      'token' => $token,
      '_serialize' => array('token', 'message')
    ));
  }

  /**
   * POST /users/password.json
   * This action receive a json with current password, new password, and repeated new password,
   * to update the user password.
   */
  public function password() {
    $password_received = $this->request->data['password'];
    $new_password = $this->request->data['new_password'];
    $rnew_password = $this->request->data['rnew_password'];
    $token = $this->request->data['token'];

    $user = $this->User->findByToken($token);

    if(!empty($user)){
      if($password_received == $user['User']['password'] &&
          ($new_password == $rnew_password)
      ){
        $this->User->id = $user['User']['id'];
        $this->User->save(array('password' => $new_password));
        $message = 'Password changed with success';
        $this->response->statusCode(200);
      }
      else{
        $message = 'Current password does not match';
        $this->response->statusCode(400);
      }
    }
    else{
      $message = 'User not found';
      $this->response->statusCode(404);
    }

    $this->set(array(
      'message' => $message,
      '_serialize' => array('message')
    ));
  }

  /**
   * POST /users/recover.json?token={token}
   */
  public function recover() {
    $email_received = $this->request->data['email'];
    $user = $this->User->findByEmail($email_received);

    if(!empty($user)){
      $this->sendPasswordRecoverEmail($user['User']);
      $message = 'Hello, an email with your password has been sent to'+ $user['User']['email'];
      $this->response->statusCode(200);
    }
    else{
      $message = "Sorry, we don't have an user with this email";
      $this->response->statusCode(400);
    }

    $this->set(array(
      'message' => $message,
      '_serialize' => array('message')
    ));
  }


  private function sendPasswordRecoverEmail($user){
    App::uses('CakeEmail', 'Network/Email');
    $Email = new CakeEmail();
    $Email->config('default');
    $Email->to($user['email']);
    $Email->subject('Password recovery');
    $messagePassword = "Hello " . $user['username'] . ". Your pasword is " . $user['password'] . " .Try to login at: http://fleetster.eu01.aws.af.cm/ with the password provided.";
    $Email->send($messagePassword);
  }

  private function sendNewUser($user){
    App::uses('CakeEmail', 'Network/Email');
    $Email = new CakeEmail();
    $Email->config('default');
    $Email->to($user['email']);
    $Email->subject('Welcome to Fleetster trainning');
    $messageBody = "Hello " . $user['username'] . ". Try to login at: http://fleetster.eu01.aws.af.cm/ with the password provided";
    $Email->send($messageBody);
  }


}
