The backend is done with CakePHP + MySQL. The architecture for integration is REST, and json is used to pass/receive messages. 

There are two resources on the backend

* User 
* Journey

Before we delve in details about resources we need to talk about authentication.

## Authentication 

Our server is Stateless. This means that we do not store sessions between requisitions, and every call to the API has to provide a token. This was a design choice.

For every call to non-public REST resource the client have to provide their credential. That is a token.

Hence, every call to the server is wraped in the following function, that is implemented on AppController

```
#!php

function authenticate($token, $context, $fn){
      $this->loadModel('User');
      $user = $this->User->findByToken($token);

      if(!empty($user)){
        $this->user = $user['User'];
        $fn($this->user, $context);
      }
      else {
        $this->response->statusCode(404);
        $this->set('message', 'User not authenticated');
        $this->set('_serialize', 'message');
      }
    }

```

If there is an user with a provided token, this function call a callbak function that execute the controller business logic.

If the token is invalid we respond with a 'User not authenticated' and 404 status (NOT FOUND)

## User

We have the following actions for this resource

#### POST /users.json

This action is used to add a new user. We generate a new token and give it to the recently created user. 
If the user is created with seccess, an email is sent to the provided email.

There is a private function to generate a random token. 

**Pitfalls**

*The token could be generated leveraging CakePHP Helper classes*  
*The password is saved as plain in the database. It could be hashed before saving, to enforce security.*

#### POST /users/auth.json

This action return the token with a HTTP 200 code if the passed json with user/password is valid. Otherwise it returns HTTP 403 (Forbidden). We decided to not let the user know what has failed because of security concerns.

*Because the API key (token) is sent to the client it need to expire from time to time. And the client have to obtain a new one. This was not implemented*

#### GET /users/show.json?token={token}  and  PUT /users/update.json?token={token}

This action is used to retrieve user profile for edition. Observe that we do not use the id. That's because we don't want to expose that to the client. After edition we call the update action with the changed json.

**Pitfalls**

*Maybe, a better approach would be use a unique url like: /users/{token}.json and change the HTTP method to GET/PUT this resource*

#### POST /users/password.json?token={token}

This action receive a json with current password, new password, and repeated new password, to update the user password.

**Pitfalls**

*If we decided to hash the password before saving. The we have to build a screnn and action to a unathenticated user with a private url change his password*

#### POST /users/recover.json?token={token}

This action recover for a provided email. This action has to be a post to avoid accidentally call to this resource be it from a crawler or another source.

## Other observations for User

We use Mailgun to send email. There is two private actions on the UserController that use this configuration to communicate with the user (POST /users.json and POST /users/recover.json)

## Journey

Because this resource is private, every call is wraped using the authenicate function.  
This resource follow more REST conventions. Let's see its actions.

Every call consider the calling user, as expected.

#### GET /journeys.json?token={token}  

This is a list actually.

#### GET /journeys/{id}.json?token={token}
#### PUT /journeys/{id}.json?token={token}
#### POST /journeys/{id}.json?token={token}
#### DELETE /journeys/{id}.json?token={token}

## Models

There are two models that corresponds to this resources User and Journey.  
A Journey belongs to a User.  
  
*Pitfalls*

*Theoretically, an User hasMany Journeys but I had to remove this relation because cakePHP do not lazily fetch the hasMany relation. So when my user logs in, all Journeys were being fetched. Obviously there is performance penalties with this approach. At the other side, removing hasMany relationship make more difficult to retrieve Journeys for a defined user*  
  
*I do not validate models on server yet, expect for Journey date*
