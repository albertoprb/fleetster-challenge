/*global define*/
define([
  'underscore',
  'backbone',
  'common'
], function (_, Backbone, Common) {
  'use strict';

  var UserModel = Backbone.Model.extend({
    urlRoot: Common.baseurl + 'users.json',

    defaults: {
      username: '',
      email:'',
      password: '',
      license_id: '',
      license_expire_at: '',
      license_special_conditions: ''
    }

  });

  return UserModel;
});
