/*global define*/
define([
  'jquery',
  'underscore',
  'backbone',
  'serialize',
  'gritter',
  'text!templates/journey/listItem.html',
  'common'
], function ($, _, Backbone, Serializer, Gritter, journeyListItemTemplate, Common) {
  'use strict';

  var JourneyListItemView = Backbone.View.extend({

    tagName: 'tr',

    template: _.template(journeyListItemTemplate),

    render: function() {
        var $el = $(this.el);
        $el.html(_.template(journeyListItemTemplate, this.model.attributes));
        return this;
    },


    events:{
      "click a[name='delete']":"del"
    },

    /**
     * Handle delete
     * @param e
     */
    del:function (e) {
      e.preventDefault();
      var id = $(e.target).attr('data-id');
      var element = $(e.target).closest('tr');
      if (confirm('Are you sure you want to remove this journey?')) {
        this.model.url = this.model.urlRoot + '/' + this.model.id + '.json?token=' + Common.token;
        this.model.destroy({
              success:function(collection, response){
                  console.log("Deleted Journey");
                  element.remove();
              }
          });
      }
    }




  });

  return JourneyListItemView;
});
