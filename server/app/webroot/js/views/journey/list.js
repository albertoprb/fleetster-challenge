/*global define*/
define([
  'jquery',
  'underscore',
  'backbone',
  'serialize',
  'gritter',
  'collections/journeys',
  'text!templates/journey/list.html',
  'views/journey/listItem',
  'common'
], function ($, _, Backbone, Serializer, Gritter,
             JourneyCollection, journeyListTemplate, JourneyListItemView,
             Common) {
  'use strict';

  var JourneyListView = Backbone.View.extend({

    el: '#page-content',

    template: _.template(journeyListTemplate),

    render: function() {
      this.$el.html(this.template);
      /**
       * For every model on our collection we create a view and render it.
       */
      _.each(this.collection.models, function (journey) {
        this.renderJourney(journey);
      }, this);
      return this;
    },

    renderJourney: function(model) {
      var item = new JourneyListItemView({ model: model });
      $('#journey_table').append(item.render().el);
    },

    events:{
      "click #addJourney":"create"
    },

    create:function () {
      window.location.hash = 'journey/new';
    }


  });

  return JourneyListView;
});
