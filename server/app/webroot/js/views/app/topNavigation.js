/*global define*/
define([
  'jquery',
  'underscore',
  'backbone',
  'serialize',
  'gritter',
  'text!templates/app/topNavigation.html',
  'common',
   'routers/router'
], function ($, _, Backbone, Serializer, Gritter, topNavigationTemplate, Common) {
  'use strict';

  var TopNavigation = Backbone.View.extend({

    el: '#topNavigation',

    template: _.template(topNavigationTemplate),

    render: function() {
      this.$el.html(this.template({username:this.model}));
      return this;
    },

    events:{
      "click a[href='#profile']":'profile',
      "click a[href='#logout']":'logout'
    },

    profile:function () {
        window.location.hash = 'profile';
    },

    logout:function () {
      Common.token = Common.standard_token; // Invalidate token. This is not so useful because of the subsequent redirect
      window.location.href = Common.baseurl;
    }

  });

  return TopNavigation;
});
