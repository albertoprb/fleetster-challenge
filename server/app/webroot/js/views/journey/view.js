/*global define*/
define([
  'jquery',
  'underscore',
  'backbone',
  'serialize',
  'gritter',
  'text!templates/journey/view.html',
  'models/journey',
  'datepicker',
  'validation',
  'common'
], function ($,_, Backbone, Serializer, Gritter, journeyViewTemplate, Journey, datepicker, validation, Common) {
  'use strict';

  var JourneyView;
  JourneyView = Backbone.View.extend({

    el:'#page-content',

    template:_.template(journeyViewTemplate),


    initialize:function () {
      /**
       * If the journey is new we create a model.
       */
      if (this.id === 'new') {
        console.log('New Journey');
        this.model = new Journey({token:Common.token});
        this.model.url = this.model.urlRoot + '.json?token=' + Common.token;
      }
      /**
       * If the journey already exist (Edit), we fetch it from server to render the view
       */
      else {
        console.log('GET journey.json from server with id ' + this.id);
        var self = this; // we have to use this to prevent context confusion on callback
        this.model = new Journey({id:this.id, token:Common.token});
        this.model.url = this.model.urlRoot + '/' + this.id + '.json?token=' + Common.token;
        this.model.fetch({
          success:function(){
              console.log(self.model);
              self.render();
          }
        });
      }
    },

    render:function () {
      this.$el.html(this.template(this.model.attributes));
      $('select').val(this.model.attributes['status']);

      this.$el.find('#save_journey').validate(this.validationRules);

      return this;
    },

    events:{
      "click .cancel":"cancel",
      "click .save":"save"
    },

    cancel:function () {
      window.location.hash = 'journeys';
    },

    /**
     * Save action on the server
     * @param e
     */
    save:function (e) {
      e.preventDefault();
      if(!$('#save_journey').valid()){
        $('.error').first().find('input').focus()
      } else {
        var data = Serializer.serialize(this);
        this.model.set(data);
        console.log('Preparing to save journey');

        this.model.save(null, {
          success:function (model, response, options) {
            console.log('Journey saved');
            console.log(response);

            // Clean form
            $("input").val('');
            $.gritter.add({title:'Success!', text:'Your journey was successfully saved.'});
            window.location.hash = 'journeys';
          },
          error:function (model, errors) {
            console.log('Server Refused Journey - invalid');
          }
        });
      }
    },

    /**
     * Validation Options for journey
     */
    validationRules: {
      errorElement: 'label', //default input error message container
      errorClass: 'help-inline', // default input error message class
      rules: {
        date: {
          required: true,
          dateISO: true // Standard date for this application
        },
        from: {
          required: true
        },
        to: {
          required: true
        },
        initialMileage: {
          digits:true
        },
        finalMileage: {
          digits:true
        }
      },
      messages: {
        date: {
          required: "The planned date for your journey is required.",
          dateISO: "Please enter a valid date."
        },
        from: {
          required: "The departure location is required."
        },
        to: {
          required: "The destination location is required."
        },
        initialMileage: {
          digits:"The initial mileage must be a number."
        },
        finalMileage: {
          digits:"The final mileage must be a number."
        }
      },
      highlight: function (element) {
        $(element).closest('.control-group').removeClass('success').addClass('error');
      },

      success: function (element) {
        element.closest('.control-group').removeClass('error').addClass('success');
        element.remove();
      },

      errorPlacement: function (error, element) {
        error.addClass('help-small no-left-padding controls').insertAfter(element.closest('.controls'));
      }
    }
  });

  return JourneyView;
});
