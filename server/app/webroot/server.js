var express = require('express')
  , http = require('http')
  , path = require('path');

var app = express();

app.configure(function(){
  app.set('port', process.env.VCAP_APP_PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, './')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', function(req, res){
  res.sendfile(path.join(__dirname, './login.html'), {root:'/'});
});

app.post('/users/auth.json', function(req, res){
    console.log(req.body);
    res.send(req.body);
});

app.post('/users/recover.json', function(req, res){
    console.log(req.body);
    res.send(req.body);
});

app.post('/users.json', function(req, res){
    console.log(req.body);
    res.send(req.body);
});

app.post('/journeys.json', function(req, res){
    req.body.id = 1;
    console.log(req.body);
    res.send(req.body);
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
