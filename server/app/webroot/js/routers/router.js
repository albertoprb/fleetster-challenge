/*global define*/
define([
  'jquery',
  'backbone',
  'common',
  'views/app/topNavigation',
  'views/user/login',
  'views/user/signup',
  'views/user/forgot',
  'views/user/profile',
  'views/journey/list',
  'views/journey/view',
  'models/user',
  'collections/journeys'
], function ($, Backbone, Common,
             TopNavigationView,
             LoginView, SignupView, ForgotPasswordView, ProfileView,
             JourneyListView, JourneyView, User, JourneyCollection) {

  'use strict';

  var AppRouter = Backbone.Router.extend({
    currentView: null,

    routes: {
      'user/:token': 'runApp',
      "login": "login",
      "signup": "signup",
      "forgot": "forgot",
      "profile": "profile",
      "journeys":"journeys",
      "journey/:id":"journey",
      'runApp':'runApp' // Used only for development with a valid token
    },

    /**
     * Set new view and remove events from the previous one. After this it render the new view.
     * @param view
     */
    changeView: function(view) {
      if ( null != this.currentView ) {
        this.currentView.undelegateEvents();
        this.currentView.$el.html('');
      }
      this.currentView = view;
      this.currentView.render();
    },

    index:function(){
      this.login();
    },

    /**
     * Return if the current token is not the standard (unauthenticated)
     * @returns {boolean}
     */
    isAuthenticated:function(){
      return (Common.token !== undefined) && (Common.token !== Common.standard_token)
    },

    /**
     * If the user is not authenticated we redirect it to the index.html
     * An user is retrieved when the app is run. The result is stored in the Common
     * It is used all over the app for authentication with the server.
     * @param token
     */
    runApp: function runApp(token) {
      //  Setting token
      Common.token = token;

      if(!this.isAuthenticated()){
        window.location.href = Common.baseurl;
      }
      else{
        // Getting user for all "SESSION"
        $.ajax({
          type:"GET",
          url: Common.baseurl + "users/show.json?token=" + Common.token,
          success:function(data){
            console.log('Success getting user');
            Common.user = new User(data);
            console.log('User is authenticated with token: '+ Common.token);
            //  Layout Views
            new TopNavigationView({model:data.username}).render();
          },
          error:function(data){
            console.log('Error getting user');
            console.log(data);
            $.gritter.add({title: 'Error getting user!', text: 'User is not authenticated'});
          }});
      }
      // Initial view for application
      this.journeys();
    },

    login: function() {
      this.changeView(new LoginView());
    },

    forgot: function() {
      this.changeView(new ForgotPasswordView());
    },

    signup: function() {
      this.changeView(new SignupView());
    },

    profile: function() {
      this.changeView(new ProfileView());
    },

    /**
     * This is the main screen
     */
    journeys: function(){
      var self = this;
      var journeyCollection = new JourneyCollection();
      journeyCollection.url += Common.token;
      journeyCollection.fetch({
        cache: false,
        success:function(collection, response){
          console.log("Received Journeys");
          self.changeView(new JourneyListView({collection:journeyCollection}));
        }
      });
    },

    journey: function(id){
      this.changeView(new JourneyView({'id':id}));
    }
  });

  return AppRouter;
});
