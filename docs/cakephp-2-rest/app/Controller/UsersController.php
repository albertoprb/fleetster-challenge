<?php
class UsersController extends AppController {

    public function index() {
        $users = $this->User->find('all');
        $this->set('users', Set::map($users));
        $this->set('_serialize', 'users');
    }

    public function view($id) {
        $user = $this->User->findById($id);
        $this->set('user', Set::map($user));
        $this->set('_serialize', 'user');
    }

    // public function add() {
    //     $saved = $this->Book->save($this->request->data);

    //     if ($saved) {
    //         $message = 'Saved';
    //         $id = $this->Book->getLastInsertId();

    //     } else {
    //         $message = 'Error';
    //         $id = null;
    //     }

    //     $this->set(array(
    //         'message' => $message,
    //         'id' => $id,
    //         '_serialize' => array('message', 'id')
    //     ));
    // }

    // public function edit($id) {
    //     $this->Book->id = $id;
    //     if ($this->Book->save($this->request->data)) {
    //         $message = 'Saved';
    //     } else {
    //         $message = 'Error';
    //     }
    //     $this->set(array(
    //         'message' => $message,
    //         '_serialize' => array('message')
    //     ));
    // }

    // public function delete($id) {
    //     if ($this->Book->delete($id)) {
    //         $message = 'Deleted';
    //     } else {
    //         $message = 'Error';
    //     }
    //     $this->set(array(
    //         'message' => $message,
    //         '_serialize' => array('message')
    //     ));
    // }
}
