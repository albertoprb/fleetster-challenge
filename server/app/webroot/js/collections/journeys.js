/*global define*/
define([
  'underscore',
  'backbone',
  'backboneLocalstorage',
  'models/journey',
  'common'
], function (_, Backbone, Store, Journey, Common) {
  'use strict';

  var JourneyCollection = Backbone.Collection.extend({
    url: Common.baseurl + 'journeys.json?token=' ,
    model: Journey
  });

  return JourneyCollection;
});
