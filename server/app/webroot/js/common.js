/*global define*/
'use strict';

define([], function () {
  return {

    /**
     * We store glocal configs here
     */

    standard_token: 'not-authorized',
    token: 'not-authorized',



    /**
    * Development
    */
    //baseurl: 'http://localhost:3000/', /** Node Frontend */
//    baseurl: 'http://localhost/fleetster/',
//    path: '/fleetster/'

      /**
    * Production
    */
    baseurl: '/',
    path: '/'
  };
});
