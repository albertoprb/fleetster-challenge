/*global define*/
define([
  'jquery',
  'underscore',
  'backbone',
  'serialize',
  'gritter',
  'text!templates/user/profile.html',
  'datepicker',
  'validation',
  'models/user',
  'common'
], function ($, _, Backbone, Serializer, Gritter, profileTemplate, datepicker, validation, User, Common) {
  'use strict';

  var ProfileView = Backbone.View.extend({

    el: '#page-content',

    render: function() {
      this.$el.html(_.template(profileTemplate, this.model.attributes)) ;
      $('textarea').val(this.model.attributes['license_special_conditions']);
      this.$el.find('#personal_information').validate(this.personalInfoFormValidationOptions); // activate validation
      this.$el.find('#change_password').validate(this.passwordFormValidationOptions); // activate validation
      return this;
    },

    initialize: function(){
      $.extend(this.personalInfoFormValidationOptions, this.profileValidationOptions); // merge options
      $.extend(this.passwordFormValidationOptions, this.profileValidationOptions); // merge options
      this.model = Common.user; // Use the 'session' user in this view
    },

    events:{
      "click .cancel":"cancel",
      "click .save_profile":"save_profile",
      "click .change_password":"change_password"
    },

    cancel:function () {
      window.location.hash = 'journeys';
    },

    /**
     * Change profile information
     * @param e
     */
    save_profile:function (e) {
      e.preventDefault();
      if(!$('#personal_information').valid()){
        $('.error').first().find('input').focus()
      }

      else {
        var profile_data = Serializer.serialize(this);
        this.model.set(profile_data);
        profile_data.token = Common.token;
        $.ajax({
          type:"POST",
          url: Common.baseurl + "users/update.json",
          data:profile_data,
          success:function(data){
            console.log('Update local user');
            $.gritter.add({title: 'Saved Profile!', text: 'Profile information successfully saved.'});
          },
          error:function(data){
            console.log('Error updating user');
            console.log(data);
            $.gritter.add({title: 'Error', text: 'An error has occurred while saving your data'});
          }});
      }
    },

    /**
     * Change password action
     * @param e
     */
    change_password:function (e) {
      e.preventDefault();

      if(!$('#change_password').valid()){
        $('.error').first().find('input').focus()
      }
      else {
        var post_data = {
          'password': $('#password').val(),
          'new_password' : $('#new_password').val(),
          'rnew_password' : $('#rnew_password').val(),
          'token' : Common.token
        };
        $.ajax({
          type:"POST",
          url: Common.baseurl + "users/password.json",
          data:post_data,
          success:function(data){
            console.log('Updated password');
            $.gritter.add({title: 'Success', text: 'Your password has changed.'});
          },
          error:function(data){
            console.log('Error updating user');
            console.log(data);
            $.gritter.add({title: 'Error', text:JSON.parse(data.responseText).message});
          }});
      }
    },

    /**
     * Common validation options
     */
    profileValidationOptions: {
      highlight: function (element) {
        $(element).closest('.control-group').removeClass('success').addClass('error');
      },

      success: function (element) {
        element.closest('.control-group').removeClass('error').addClass('success');
        element.remove();
      },

      errorPlacement: function (error, element) {
        error.addClass('help-small no-left-padding').insertAfter(element.closest('.controls'));
      }
    },

    /**
     * Validation options specific to profile form
     */
    personalInfoFormValidationOptions: {
      errorElement: 'label', //default input error message container
      errorClass: 'help-inline', // default input error message class
      rules: {
        email: {
          required: true,
          email: true
        },
        expire_at: {
          dateISO: true
        },
        license_id: {
          digits: true
        }
      },
      messages: { // custom messages for radio buttons and checkboxes
        email: {
          required: "Email is required.",
          email: "Please enter a valid email."
        },
        expire_at: {
          dateISO: "Please enter a valid date."
        },
        license_id: {
          digits: "The license ID can only contain digits."
        }
      }
    },

    /**
     * Validation options specific to change password form
     */
    passwordFormValidationOptions: {
      errorElement: 'label', //default input error message container
      errorClass: 'help-inline', // default input error message class
      rules: {
        password: {
          required: true
        },
        new_password: {
          required: true
        },
        rnew_password: {
          required: true,
          equalTo: "#new_password"
        }
      },
      messages: {
        password: {
          required: "Your current password is required."
        },
        new_password: {
          required: "Your new password is required."
        },
        rnew_password: {
          required: "Please repeat your new password.",
          equalTo: "Passwords must be the same"
        }
      }
    }

  });

  return ProfileView;
});
