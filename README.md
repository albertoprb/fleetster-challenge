# Fleetster Trainning Project

### Specs

Take a look at spec-wireframe.pdf and spec.md files in docs/

### Architecture

The architecture is the combination of a full REST backend in CakePHP and a Frontend in Backbone/Jquery. Pllease read the backend part first to understand better how frontend consumed backend services.

* [Backend](server/)
* [Frontend](server/webroot/)

For the visual part HTML/CSS structure I used Metronic template.


