/*global define*/
define([
  'jquery',
  'underscore',
  'backbone',
  'serialize',
  'gritter',
  'models/user',
  'text!templates/user/form-forgot-password.html',
  'validation',
  'common'
], function ($, _, Backbone, Serializer, Gritter, User, forgotPasswordTemplate, validation, Common) {
  'use strict';

  var ForgotPasswordView = Backbone.View.extend({

    el: '#forgot-password-form',

    template: _.template(forgotPasswordTemplate),

    render: function() {
      this.$el.html(this.template);
      this.$el.find('.forget-form').validate(this.validationOps); // activate validation
      return this;
    },

    events: {
      "click #back-btn" : "goToLogin",
      "submit form": "recover"
    },

    goToLogin: function () {
      window.location.hash = 'login';
    },

    /**
     * Recover password
     * @param e
     */
    recover: function (e) {
      e.preventDefault();

      $.gritter.add({title: 'Recovering...', text: 'Trying to recover your password...'});
      $.ajax({
        type:"POST",
        url: Common.baseurl + "users/recover.json",
        data:
        {
          email: $('input[name=email]').val()
        },
        success:function(data){
          console.log('Success');
          console.log(data);
          $('input').val("");
          $.gritter.add({title: 'Password recovery', text: 'Your password was sent to your email. Please check it!'});
          window.location.hash = 'login';
        },
        error:function(data){
          console.log('Error');
          var responseText =  JSON.parse(data.responseText);
          console.log(responseText.message);
          $.gritter.add({title: 'Error!', text: responseText.message});
        }});
    },

    /**
     * Form validation options
     */
    validationOps: {
      errorElement: 'label', //default input error message container
      errorClass: 'help-inline', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "",
      rules: {
        email: {
          required: true,
          email: true
        }
      },

      messages: {
        email: {
          required: "Email is required.",
          email: "Please enter a valid email."
        }
      },

      invalidHandler: function (event, validator) { //display error alert on form submit

      },

      highlight: function (element) { // hightlight error inputs
        $(element)
            .closest('.control-group').addClass('error'); // set error class to the control group
      },

      success: function (label) {
        label.closest('.control-group').removeClass('error');
        label.remove();
      },

      errorPlacement: function (error, element) {
        error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
      }
    }
  });

  return ForgotPasswordView;
});
