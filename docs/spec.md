Specification
=================

Web application allowing drivers to register with username, password and car details. After that they could define journeys.

Please see the attached Wireframes

Features
-----------------

### Driver

* Login
* Signup
* (Bonus) Password Recover
* (Bonus) Send email confirmation

### Car

* CRUD

### Journey

A Car "has many" Journeys

* Start Journey (starting and destination locations)
* End Journey (input final mileage)


Technical constraints
---------------------

### Frontend

* Backbone.js
* Single Page App (Bonus)
* Knockout (Bonus)

### Backend

* CakePHP
* REST Server
* MySQL

Design considerations
---------------------

You can use app.fleetster.de as a graphical baseline.

* Consider using bootstrap or zurb foundation or grummy