<?php
App::uses('AppModel', 'Model');
/**
 * Book Model
 *
 */
class Journey extends AppModel {

    public $belongsTo = 'User';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'date';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate;

    public function __construct($id = false, $table = null, $ds = null) {
        $this->validate = array(
            'date' => array(
                'minlength' => array(
                    'rule' => array('minlength', 1),
                    'message' => 'Title must be at least one character long',
                    //'allowEmpty' => false,
                    //'required' => true,
                    //'last' => false, // Stop validation after this rule
                    //'on' => 'create', // Limit validation to 'create' or 'update' operations
                )
            )
        );
        parent::__construct($id, $table, $ds);
    }
}
