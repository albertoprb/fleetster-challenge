/*global define*/
define([
  'underscore',
  'backbone',
  'common'
], function (_, Backbone, Common) {
  'use strict';

  var JourneyModel = Backbone.Model.extend({

    urlRoot: Common.baseurl + 'journeys',

    defaults: {
      date:'',
      from: '',
      to:'',
      initialMileage:'',
      finalMileage:'',
      status: ''
    }
  });

  return JourneyModel;
});
