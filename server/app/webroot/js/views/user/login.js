/*global define*/
define([
  'jquery',
  'underscore',
  'backbone',
  'serialize',
  'gritter',
  'text!templates/user/form-login.html',
  'validation',
  'common'
], function ($, _, Backbone, Serializer, Gritter, loginTemplate, validation, Common) {
  'use strict';

  var LoginView = Backbone.View.extend({

    el: '#login-form',

    template: _.template(loginTemplate),

    render: function() {
      this.$el.html( _.template(loginTemplate, this.model));
      this.$el.find('.login-form').validate(this.validationOps); //activate validation
      return this;
    },

    events: {
      "submit form": "login"
    },

    /**
     * Login user
     * @param e
     */
    login: function (e) {
      e.preventDefault();

      console.log('Preparing to login user');
      $.gritter.add({title: 'Logging in...', text: 'Trying to login with the provided credentials'});

      $.ajax({
        type:"POST",
        url: Common.baseurl + "users/auth.json",
        data:
        {
          username: $('input[name=username]').val(),
          password: $('input[name=password]').val()
        },
        success:function(data){
          console.log('Success');
          console.log(data);
          $.gritter.add({title: 'Success!', text: data.message});
          /**
           * If the login is successfull we redirect user to the app with his token
           */
          Common.token = data.token;
          window.location.href = Common.baseurl + "app.html#user/" + data.token;
        },
        error:function(data){
          console.log('Error');
          var responseText =  JSON.parse(data.responseText);
          console.log(responseText.message);
          $.gritter.add({title: 'Error!', text: responseText.message});
        }});
    },

    /**
     * Login form validation rules
     */
    validationOps: {
      errorElement: 'label', //default input error message container
      errorClass: 'help-inline', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      rules: {
        username: {
          required: true
        },
        password: {
          required: true
        },
        remember: {
          required: false
        }
      },

      messages: {
        username: {
          required: "Username is required."
        },
        password: {
          required: "Password is required."
        }
      },

      invalidHandler: function (event, validator) { //display error alert on form submit
        $('.alert-error', $('.login-form')).show();
      },

      highlight: function (element) { // hightlight error inputs
        $(element)
            .closest('.control-group').addClass('error'); // set error class to the control group
      },

      success: function (label) {
        label.closest('.control-group').removeClass('error');
        label.remove();
      },

      errorPlacement: function (error, element) {
        error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
      }
    }
  });

  return LoginView;
});
