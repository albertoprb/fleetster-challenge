<?php
class JourneysController extends AppController {

  public function index() {
    $this->authenticate($this->request->query['token'], $this, function($user, $context){
      $journeys = $context->Journey->findAllByUserId($user['id']);
      $context->set('journeys', Set::map($journeys));
      $context->set('_serialize', 'journeys');
    });
  }

  public function add() {
    $this->authenticate($this->request->query['token'], $this, function($user, $context){
      $context->request->data['user_id'] = $user['id'];
      $saved = $context->Journey->save($context->request->data);

      if ($saved) {
        $context->response->statusCode(200);
        $message = $saved['Journey'];
      }
      else {
        $context->response->statusCode(500);
        $message = 'Error adding Journey';
      }

      $context->set(array(
        'message' => $message,
        '_serialize' => 'message'
      ));
    });
  }

  public function view($id) {
    $this->id = $id;
    $this->authenticate($this->request->query['token'], $this, function($user, $context){
      $journey = $context->Journey->findById($context->id);

      if (!empty($journey)) {
        if($journey['Journey']['user_id'] == $user['id']){
          $context->response->statusCode(200);
          $message = $journey['Journey'];
        }
        else {
          $context->response->statusCode(401);
          $message = 'Not authorized';
        }
      }
      else {
        $context->response->statusCode(404);
        $message = 'Journey not found';
      }

      $context->set(array(
        'message' => $message,
        '_serialize' => 'message'
      ));

    });
  }

  public function edit($id) {
    $this->id = $id;
    $this->authenticate($this->request->query['token'], $this, function($user, $context){

      $journey = $context->Journey->findById($context->id);

      if (!empty($journey)) {
        if($journey['Journey']['user_id'] == $user['id']){

          $context->Journey->id = $context->id;
          $saved = $context->Journey->save($context->request->data);

          if ($saved) {
            $context->response->statusCode(200);
            $message = $saved['Journey'];
          }
          else {
            $message = 'Error updating Journey';
          }

        }
        else {
          $context->response->statusCode(401);
          $message = 'Not authorized';
        }
      }
      else {
        $context->response->statusCode(404);
        $message = 'Journey not found';
      }


      $context->set(array(
        'message' => $message,
        '_serialize' => 'message'
      ));

    });
  }

  public function delete($id) {
    $this->id = $id;
    $this->authenticate($this->request->query['token'], $this, function($user, $context){

      $journey = $context->Journey->findById($context->id);

      if (!empty($journey)) {
        if($journey['Journey']['user_id'] == $user['id']){

          $saved = $context->Journey->delete($context->id);

          if ($saved) {
            $context->response->statusCode(200);
            $message = 'Journey deleted';
          }
          else {
            $message = 'Error updating Journey';
          }

        }
        else {
          $context->response->statusCode(401);
          $message = 'Not authorized';
        }
      }
      else {
        $context->response->statusCode(404);
        $message = 'Journey not found';
      }


      $context->set(array(
        'message' => $message,
        '_serialize' => 'message'
      ));

    });
  }


}
