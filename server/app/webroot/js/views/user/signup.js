/*global define*/
define([
  'jquery',
  'underscore',
  'backbone',
  'serialize',
  'gritter',
  'models/user',
  'text!templates/user/form-signup.html',
  'validation',
  'common'
], function ($, _, Backbone, Serializer, Gritter, User, signupTemplate, validation, Common) {
  'use strict';

  var SignupView = Backbone.View.extend({

    el: '#signup-form',

    template: _.template(signupTemplate),

    initialize: function(){
      this.model = new User();
    },

    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      this.$el.find('.register-form').validate(this.validationOps);
      return this;
    },

    events: {
      "click #register-back-btn" : "goToLogin",
      "submit form": "register"
    },

    goToLogin: function () {
      window.location.hash = 'login';
    },

    /**
     * Register action (Signup)
     * @param e
     */
    register: function (e) {
      e.preventDefault();
      var data = Serializer.serialize(this);
      this.model.set(data);
      console.log('Preparing to login user');

      this.model.save(null,{
        success:function (model, response, options){
          console.log('User created login');
          console.log(response);

          // Clean form
          $("input").val('');
          $.gritter.add({title: 'Success!', text: 'Your account was successfully created. Please check your email!'});
          window.location.hash = 'login';
        },
        error:function (model, errors){
          console.log('Server Refused User - invalid');
        }
      });
      $.gritter.add({title: 'Sending...', text: 'Your account is being created...'});
    },

    /**
     * Validation options
     */
    validationOps: {
      errorElement: 'label', //default input error message container
      errorClass: 'help-inline', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: "",
      rules: {
        username: {
          required: true
        },
        password: {
          required: true
        },
        rpassword: {
          equalTo: "#register_password"
        },
        email: {
          required: true,
          email: true
        },
        tnc: {
          required: true
        }
      },

      messages: { // custom messages for radio buttons and checkboxes
        tnc: {
          required: "Please accept TNC first."
        }
      },

      invalidHandler: function (event, validator) { //display error alert on form submit

      },

      highlight: function (element) { // hightlight error inputs
        $(element)
            .closest('.control-group').addClass('error'); // set error class to the control group
      },

      success: function (label) {
        label.closest('.control-group').removeClass('error');
        label.remove();
      },

      errorPlacement: function (error, element) {
        if (element.attr("name") == "tnc") { // insert checkbox errors after the container
          error.addClass('help-small no-left-padding').insertAfter($('#register_tnc_error'));
        } else {
          error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
        }
      }
    }
  });

  return SignupView;
});
